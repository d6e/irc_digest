use serde::{Deserialize, Serialize};
use std::cmp::PartialEq;
use std::fs::File;
use std::io::{prelude::*, BufReader, BufWriter, Lines, SeekFrom};
use std::path::{Path, PathBuf};
use std::time::SystemTime;

#[derive(Deserialize, Serialize, PartialEq, Clone, Debug)]
struct FileProperties {
    filename: String,
    last_position: u64,
    last_accessed: SystemTime,
    last_modified: SystemTime,
}

impl FileProperties {
    pub fn new(file_name: String) -> FileProperties {
        FileProperties {
            filename: file_name,
            last_position: 0,
            last_modified: SystemTime::UNIX_EPOCH,
            last_accessed: SystemTime::UNIX_EPOCH,
        }
    }
}

#[derive(Deserialize, Serialize, Default, Clone)]
struct SavedState {
    properties: Vec<FileProperties>,
}

#[test]
fn test_add_or_replace() {
    let mut s = SavedState::default();
    s.add_or_replace(FileProperties::new("boop".to_owned()));
    s.add_or_replace(FileProperties::new("beep".to_owned()));
    let mut m = FileProperties::new("boop".to_owned());
    m.last_modified = SystemTime::now();
    s.add_or_replace(m.clone());
    let expected = vec![m, FileProperties::new("beep".to_owned())];
    assert_eq!(expected, s.properties);
}

impl SavedState {
    // Adds a FileProperties. Replaces if it already exists.
    pub fn add_or_replace(&mut self, props: FileProperties) {
        match self
            .properties
            .iter()
            .position(|x| *x.filename == props.filename)
        {
            Some(index) => {
                // Exists, replace
                self.properties.remove(index);
                self.properties.insert(index, props);
            }
            None => self.properties.push(props), // Doesn't exist, append
        }
    }

    // Returns the first FileProperties that matches
    pub fn get(&self, filename: &str) -> Option<&FileProperties> {
        for f in &self.properties {
            if f.filename == filename {
                return Some(f);
            }
        }
        None
    }

    pub fn read(properties_file: &Path) -> Option<SavedState> {
        if properties_file.exists() {
            let file = File::open(properties_file).unwrap();
            let reader = BufReader::new(file);
            let json = serde_json::from_reader(reader).unwrap();
            Some(json)
        } else {
            None
        }
    }

    pub fn write(self, properties_file: &Path) {
        let writer = BufWriter::new(File::create(properties_file).unwrap());
        serde_json::to_writer_pretty(writer, &self).unwrap();
    }
}

fn read(filename: PathBuf, mut props: FileProperties) -> (Lines<BufReader<File>>, FileProperties) {
    let mut file = File::open(&filename).unwrap();
    let meta = file.metadata().unwrap();
    let length = file.seek(SeekFrom::End(0)).unwrap();
    let last_accessed = meta.accessed().unwrap();
    let last_modified = meta.modified().unwrap();
    let was_accessed = last_accessed > props.last_accessed;
    let was_modified = last_modified > props.last_modified;
    let size_changed = length != props.last_position;
    let file_has_changed = was_accessed || was_modified || size_changed;
    if !file_has_changed {
        file.seek(SeekFrom::Start(props.last_position)).unwrap();
        let reader = BufReader::new(file);
        return (reader.lines(), props);
    }
    if length < props.last_position {
        eprintln!("WARNING: The file {:?} has been deleted, recreated, or something. Reading the file from the beginning.", filename);
        props.last_position = 0; // reset to the beginning
    }
    // The file has been updated since we last tried to read,
    // start from the last position and read the lines.
    file.seek(SeekFrom::Start(props.last_position)).unwrap();
    let reader = BufReader::new(file);
    props.last_accessed = last_accessed;
    props.last_modified = last_modified;
    props.last_position = length;
    (reader.lines(), props)
}

// TODO: A new file properties should be saved for every filename and tail_file should see if the file it's supposed to be tailing
// already has state.

pub fn tail_file(filename: PathBuf) -> Lines<BufReader<File>> {
    let filename_str = filename.to_str().unwrap();
    let properties_file = Path::new("irc_digest_state.json");
    let mut saved_state: SavedState = match SavedState::read(properties_file) {
        Some(p) => p, // if found return
        None => {
            let mut new = SavedState::default();
            new.add_or_replace(FileProperties::new(filename_str.to_owned()));
            new
        } // if not found, return new instance
    };
    let file_property = saved_state.get(filename_str).unwrap();
    let old_props: FileProperties = file_property.clone();

    let (opt_lines, new_props) = read(filename, file_property.clone());
    // Check if the properties changed
    if old_props != new_props {
        saved_state.add_or_replace(new_props);
        saved_state.write(properties_file);
        // TODO: Cache the old_props so we don't always have to read from file
    }
    opt_lines
    // thread::sleep(time::Duration::from_millis(1000));
}
