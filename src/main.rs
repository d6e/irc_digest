#[macro_use]
extern crate lazy_static;
use clap::{App, Arg};
use warp::Filter;
mod tail;
use atom_syndication::{ContentBuilder, Entry, EntryBuilder, Feed, FeedBuilder};
use chrono::{prelude::*, DateTime, Duration, NaiveDate, NaiveDateTime, NaiveTime, TimeZone, Utc};

use glob::glob;
use maplit::hashset;
use regex::Regex;
use std::io::sink;
use std::path::{Path, PathBuf};
use urlencoding;

fn build_entry(content_value: String) -> Result<Entry, String> {
    ContentBuilder::default()
        .content_type("xhtml".to_string())
        .value(content_value)
        .build()
        .and_then(|content| {
            EntryBuilder::default()
                .title("entry4".to_owned())
                .content(content)
                .build()
        })
}

#[test]
fn test_locate_url1() {
    let input =
        "rljsakldfj lf [http://example.com/boop/beep?url=rawr lskdfj http://example.com/short";
    let offset = 50;
    let (mut start, mut end) = locate_url(&input[offset..]).unwrap();
    start += offset;
    end += offset;
    assert_eq!("http://example.com/short", &input[start..end]);
}

#[test]
fn test_locate_url2() {
    let input =
        "rljsakldfj lf [http://example.com/boop/beep?url=rawr lskdfj http://example.com/short";
    let (start, end) = locate_url(input).unwrap();
    assert_eq!("http://example.com/boop/beep?url=rawr", &input[start..end]);
}

lazy_static! {
    static ref URL: Regex =
        Regex::new(r"(http)s?(://)[A-Z,a-z,0-9,-,.,_,~,:,/,?,#,[,],@,!,$,&,',(,),*,+,;,%,=]*")
            .unwrap();
}

fn locate_url(input: &str) -> Option<(usize, usize)> {
    match URL.find(&input) {
        Some(res) => {
            let start = res.start();
            let end = res.end();
            Some((start, end))
        }
        None => None,
    }
}

#[test]
fn test_href_ify_urls1() {
    let input =
        "lksdjfl http://example.com/rawr/long/url lsjdf http://example.com/short jdf".to_owned();
    let output = href_ify_urls(input);
    assert_eq!("lksdjfl <a href=\"http://example.com/rawr/long/url\">http://example.com/rawr/long/url</a> lsjdf <a href=\"http://example.com/short\">http://example.com/short</a> jdf", output);
}

#[test]
fn test_href_ify_urls2() {
    let input = "2017-11-20 18:28:44     kuudes  well, https://www.nature.com/articles/npp199317"
        .to_owned();
    let output = href_ify_urls(input);
    assert_eq!("2017-11-20 18:28:44     kuudes  well, <a href=\"https://www.nature.com/articles/npp199317\">https://www.nature.com/articles/npp199317</a>", output);
}

fn href_ify_urls(mut input: String) -> String {
    let mut prev_start: usize = 0;
    while prev_start < input.len() {
        let opt = locate_url(&input[prev_start..]);
        if opt.is_none() {
            break;
        }
        let (mut start, mut end) = opt.unwrap();
        start += prev_start;
        end += prev_start;
        let url = input[start..end].to_owned();
        println!("url={}", url);
        let prefix = "<a href=\"";
        let suffix = format!("\">{}</a>", url);
        input.insert_str(start, prefix);
        end += prefix.len();
        input.insert_str(end, &suffix);
        end += suffix.len();
        prev_start = end;
    }
    input
}

#[test]
fn test_retain_only_last_sec() {
    let mut lines = vec![
        "        2017-11-21 17:34:14     @sam  1440min earlier",
        "        2017-11-20 18:34:14     @bob  60min earlier",
        "        2017-11-20 17:35:14     @sam  1min earlier",
        "        2017-11-20 17:34:14     @bob  0min earlier",
    ];
    // let lines = get_last_sec(lines, 300);
    let format_str = String::from("%Y-%m-%d %H:%M:%S");
    retain_only_last_sec(&mut lines, &format_str);
    assert_eq!(3, lines.len());
}

fn retain_only_last_sec(lines: &mut Vec<&str>, format_str: &str) {
    let first = lines.first().unwrap();
    let timestamp = parse_timestamp(first, &format_str);
    let previous_time = timestamp
        .checked_sub_signed(Duration::seconds(300))
        .unwrap();
    lines.retain(|line| parse_timestamp(line, &format_str) < previous_time); // retain only what's happened since
}

#[test]
fn test_parse_timestamp() {
    let line = "        2017-11-20 17:34:14     @bob  'authentic'";
    let expected = Utc.ymd(2017, 11, 20).and_hms(17, 34, 14);
    let dt = parse_timestamp(line, "%Y-%m-%d %H:%M:%S");
    println!(
        "{} {}",
        expected.to_rfc3339().to_string(),
        dt.to_rfc3339().to_string()
    );
    assert_eq!(expected.to_rfc3339(), dt.to_rfc3339());
}

fn parse_timestamp(line: &str, format_str: &str) -> DateTime<Utc> {
    let mut tmp = line.trim().to_owned();
    let timestamp_len = Utc::now().format(&format_str).to_string().len();
    tmp.truncate(timestamp_len); // truncate to where the timestamp hopefully is
    match DateTime::parse_from_str(&tmp, &format_str) {
        // attempt to parse with timzone
        Ok(t) => t.with_timezone(&Utc),
        Err(e) => {
            eprintln!("Error={:?} value=\'{}\'", e, &tmp);
            DateTime::<Utc>::from_utc(
                NaiveDateTime::parse_from_str(&tmp, &format_str).unwrap(),
                Utc,
            )
        }
    }
}

fn rss_feed(channel: PathBuf) -> String {
    let recent_lines = tail::tail_file(channel);
    let lines: Vec<String> = recent_lines.map(|x| x.unwrap()).collect();

    // Format the entry content
    let mut content = format!("\n<p>{}</p>\n", lines.join("</p>\n<p>"));

    content = href_ify_urls(content);

    // Sanitize
    let url_schemes = hashset!["http", "https", "mailto", "magnet"];
    content = ammonia::Builder::new()
        .add_tags(&["a", "p", "href", "img"])
        .url_schemes(url_schemes)
        .clean(&content)
        .to_string();

    // Create entries for all the content
    let mut entries: Vec<Entry> = Vec::new();
    match build_entry(content) {
        Ok(x) => entries.push(x),
        Err(e) => eprintln!("{}", e),
    };

    let now = chrono::offset::Utc::now();
    let feed: Feed = FeedBuilder::default()
        .title("IRC Digest")
        .subtitle("An IRC RSS feed.".to_owned())
        .updated(now)
        .entries(entries)
        .build()
        .unwrap();
    feed.write_to(sink()).unwrap();
    feed.to_string()
}

#[test]
fn test_decode() {
    let x = "%23lesswrong";
    let channel = urlencoding::decode(&x).unwrap();
    println!("Channel={}", channel);
    assert_eq!("#lesswrong", channel)
}

#[test]
fn test_decode_fail() {
    let x = "%2&lesswrong";
    let channel = match urlencoding::decode(&x) {
        Ok(v) => v,
        Err(e) => format!("{:?}", e),
    };
    println!("Channel={}", channel);
    assert_eq!("UriCharacterError { character: \'&\', index: 2 }", channel)
}

#[tokio::main]
async fn main() {
    let matches = App::new("IRC Digest")
        .version("0.1.0")
        .author("Danielle <git@d6e.io>")
        .about("For generating IRC digests")
        .arg(
            Arg::with_name("logdir")
                .short("d")
                .long("logdir")
                .value_name("LOGDIR")
                .help("Specify the log directory to search in")
                .takes_value(true),
        )
        .get_matches();
    if let Some(_logdir) = matches.value_of("logdir") {
        let logdir = _logdir.to_string();
        let routes = warp::path!("irc" / String).map(move |channel_param: String| {
            match urlencoding::decode(&channel_param) {
                Ok(channel) => {
                    println!("Requesting channel {}", &channel);
                    let mut paths = glob(&format!("{}/*{}*", logdir, channel))
                        .expect("Failed to read glob pattern");
                    match paths.next() {
                        Some(x) => {
                            println!("Reading file {:?}", &x);
                            rss_feed(x.unwrap())
                        }
                        None => "ERROR: No log file exists".to_owned(),
                    }
                }
                Err(e) => format!("{:?}", e), // TODO: return 400 error status
            }
        });
        warp::serve(routes).run(([127, 0, 0, 1], 8000)).await;
    }
}
